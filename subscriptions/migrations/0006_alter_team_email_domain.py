# Generated by Django 3.2.9 on 2022-09-23 14:49

from django.db import migrations, models
import subscriptions.validators


class Migration(migrations.Migration):

    dependencies = [
        ('subscriptions', '0005_add_team_emails_and_seats'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='email_domain',
            field=models.CharField(blank=True, help_text='Team email domain. If set to "my-awesome-team.org", everyone with an email ending with "@my-awesome-team.org" and subdomains (e.g. "@edu.my-awesome-team.org") will be considered a member of this team.<br> Domains of common email providers are not allowed.', max_length=253, null=True, validators=[subscriptions.validators.validate_email_domain]),
        ),
    ]
